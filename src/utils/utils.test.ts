import {sum} from "./sum";
import {multiplication} from "./multiplication";

describe('Testing utils', ()=> {
    test('sum', ()=>{
        expect(sum(2,3)).toBe(5)
    })
    test('multiplication', ()=> {
        expect(multiplication(2,3)).toBe(6)
    })
})
