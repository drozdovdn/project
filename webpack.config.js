const path = require('path')
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
    mode: 'development',
    entry: path.resolve(__dirname, 'index.tsx'),
    output: {
        filename: 'index-[hash:4].js',
        path: path.resolve(__dirname, 'dist')
    },
    devServer: {
        open: true,
        port: 3000,
        historyApiFallback: true,
        host: '0.0.0.0',
        client: {
            overlay: {
                errors: true,
                warnings: false,
            },
        },
    },
    resolve: {
        extensions: ['.tsx', '.ts', '.js'],
    },
    module: {
        rules: [
            {
                test: /\.(tsx|ts)?$/,
                use: 'ts-loader',
                exclude: /node_modules/,
            },
        ],
    },
    plugins: [
        new CleanWebpackPlugin(),
        new HtmlWebpackPlugin({
            title: 'My test app',
            template: path.resolve(__dirname, 'static/index.html')
        })
    ]
}
